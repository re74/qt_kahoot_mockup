#include "animatedintro.h"
#include <QPainter>
#include <QFontDatabase>

int AnimatedIntro::rotation() const { return m_rotation; }
int AnimatedIntro::scale() const{return m_scale;}
int AnimatedIntro::getNumber() const { return number; }
void AnimatedIntro::setNumber(int newNumber) { number = newNumber; }

AnimatedIntro::AnimatedIntro(QWidget *parent) :
    QLabel(parent) {};

//void AnimatedIntro::setNumber(int number) {this->number = number;}

void AnimatedIntro::paintEvent(QPaintEvent *e)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing, true);
    float sc = 1.0 + 0.1*m_scale;
    p.save();
    p.translate(width()/2,height()/2);
    p.setPen(QColor("#864cbf"));
    p.setBrush(QColor("#25076b"));
    QRectF rect(-width()*0.6*sc/2, -height()*0.6*sc/2, width()*0.6*sc, height()*0.6*sc);
    //p.scale(m_scale, m_scale);
    p.rotate(m_rotation);
    p.drawRect(rect);
    p.setPen(QPen(Qt::white,10));
    font.setBold(true);
    font.setFamily("Helvetica");
    font.setPixelSize(height()/3);
    p.setFont(font);
    QFontMetrics fontMetrics(font);
    p.drawText(rect, Qt::AlignCenter, QString::number(number));
    p.restore();
}
