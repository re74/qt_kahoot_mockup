#ifndef PLAYER_H
#define PLAYER_H
#include <QString>

class Player
{
    QString name;
public:
    Player(QString name) : name(name) {}
    QString getName();
    void setName (QString name);
};

#endif // PLAYER_H
