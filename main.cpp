#include "startwindow.h"
#include "mainwindow.h"
#include "game.h"

#include <QStackedWidget>
#include <QVBoxLayout>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QString q_1 = "What is true?";
    QString alt_1[4];
    alt_1[0] = "5 == 5";
    alt_1[1] = "4 != 4";
    alt_1[2] = "2 <= 1";
    alt_1[3] = "1 < 1";
    Question qu_1 = Question(q_1, alt_1, 0);

    QString q_2 = "What is the output of int i = 0; int j = 1; cout << i++ + ++j; ?";
    QString alt_2[4];
    alt_2[0] = "0";
    alt_2[1] = "1";
    alt_2[2] = "2";
    alt_2[3] = "3";
    Question qu_2 = Question(q_2, alt_2, 2);

    QString q_3 = "What is the output of cout << 1 + (true != false) - (false || true); ?";

    QString alt_3[4];
    alt_3[0] = "-1";
    alt_3[1] = "2";
    alt_3[2] = "1";
    alt_3[3] = "0";
    Question qu_3 = Question(q_3, alt_3, 2);

    QString q_4 = "What is the output of cout << 7 % 3 + (true && true || false && false); ?";
    QString alt_4[4];
    alt_4[0] = "0";
    alt_4[1] = "3";
    alt_4[2] = "1";
    alt_4[3] = "2";
    Question qu_4 = Question(q_4, alt_4, 3);

    Question questions[4] = {qu_1,qu_2, qu_3, qu_4};

    Game *g = new Game(questions,4,10);
    MainWindow w(g);

    w.show();
    return a.exec();
}
