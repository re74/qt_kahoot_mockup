# Qt Kahoot mock-up



## Description

This project is an assignment in Qt course.

The app is inspired by [Kahoot](https://kahoot.it/) - an online quizz syste. The aim of the assignment is to get acquainted with the Qt.

## Usage

- install [Qt](https://www.qt.io/download-qt-installer)
- clone the repository, and open it in Qt Creator.
- Build and run the application using Qt Creator GUI.
- When the app opens, write your name. The quizz will start after you press the "Ok, go!" button:
![No saved games](pics/main_window.jpg)
- When the question appear, the timer on the left will start countdown. Press on the button with the right answer. The next question will appear automatically. The number of right answers is presenteed on the right.
![No saved games](pics/quest_window.jpg)
## Maintainers
@MariaNema
