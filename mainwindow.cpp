#include "mainwindow.h"
#include "qprogressbar.h"
#include "ui_mainwindow.h"
#include <QFontDatabase>
#include <QPainter>
#include <QPropertyAnimation>
#include <QSequentialAnimationGroup>


MainWindow::MainWindow(Game *game, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{    
    ui->setupUi(this);    
    this->game=game;
    this->timer = new QTimer();
    this->timeLine = new QTimeLine(6000, this);
    timeLine->setFrameRange(0, 100);

    this->timer->setInterval(1000);
    this->current_question=0;
    ui->stackedWidget->setCurrentIndex(0);
    animation_1 = new QPropertyAnimation(ui->label_animated_intro, "rotation");
    animation_2 = new QPropertyAnimation(ui->label_animated_intro, "scale");
    animation_group = new QSequentialAnimationGroup;
    animation_group->addAnimation(animation_1);
    animation_group->addAnimation(animation_2);
    animation_1->setStartValue(0);
    animation_1->setEndValue(360);
    animation_1->setLoopCount(1);
    animation_1->setDuration(600);
    animation_2->setStartValue(6);
    animation_2->setEndValue(1);
    animation_2->setLoopCount(1);
    animation_2->setDuration(400);
    animation_group->setLoopCount(3);
    this->ui->progressBar->setRange(0,50);
    this->ui->progressBar->setTextVisible(false);
    this->ui->progressBar->setLayoutDirection(Qt::RightToLeft);
    this->ui->progressBar->setValue(0);
    //this->ui->progressBar->setInvertedAppearance(true);
    timeLine->setFrameRange(0, 50);


    //Font
    int font_id = QFontDatabase::addApplicationFont(":/Montserrat-Bold.ttf");
    QString font_str = QFontDatabase::applicationFontFamilies(font_id).at(0);
    QFont font(font_str,QFont::Normal);
    this->ui->pushButton_start->setFont(font);
    this->ui->lineEdit->setFont(font);
    this->ui->pushButton_0->setFont(font);
    this->ui->pushButton_1->setFont(font);
    this->ui->pushButton_2->setFont(font);
    this->ui->pushButton_3->setFont(font);
    this->ui->label_question->setFont(font);
    this->ui->label_points->setFont(font);
    this->ui->label_timer->setFont(font);    
    //Button icons
    ui->pushButton_0->setIcon(QIcon(":/triangle"));
    ui->pushButton_1->setIcon(QIcon(":/circle"));
    ui->pushButton_2->setIcon(QIcon(":/romb"));
    ui->pushButton_3->setIcon(QIcon(":/square"));

    //Styling
    this->ui->stackedWidget->setStyleSheet("#page_0 { background-color: #6729d9; } #page_1 { background-color: #f2f2f2; } #page_2 { background-color: #6729d9; } #page { background-color: #6729d9;} #page_3 { background-color: #6729d9;} ");
    this->ui->pushButton_start->setStyleSheet("#pushButton_start { background-color: #383040; color: white; border-style: outset; border-width: 2px; border-radius: 10px; border-color: black; font: bold 14px; min-width: 10em; padding: 6px;}  #pushButton_start:hover { background-color: black;}");
    this->ui->lineEdit->setStyleSheet("#lineEdit { border-radius: 10px; font: bold 14px; min-width: 10em; padding: 6px;}  #lineEdit:focus { border-style: solid; border-color: black; border-width: 2px; border-radius: 10px;}");
    this->ui->progressBar->setStyleSheet("#progressBar {border-style: solid; color: #6729d9; background-color: #25076b;} #progressBar:chunk {background-color:#6729d9; } ");
    this->ui->pushButton_0->setStyleSheet("#pushButton_0 {text-align:left; background-color: #e21b3c; color: white; border-style: outset; border-width: 2px; border-radius: 3px; border-color: #c01733; font: bold 14px; min-width: 10em; padding: 6px;} #pushButton_0:hover { background-color: #d01937;}");
    this->ui->pushButton_1->setStyleSheet("#pushButton_1 {text-align:left; background-color: #d89e00; color: white; border-style: outset; border-width: 2px; border-radius: 3px; border-color: #b88600; font: bold 14px; min-width: 10em; padding: 6px;} #pushButton_1:hover { background-color: #c79200;}");
    this->ui->pushButton_2->setStyleSheet("#pushButton_2 {text-align:left; background-color: #1368ce; color: white; border-style: outset; border-width: 2px; border-radius: 3px; border-color: #1059af; font: bold 14px; min-width: 10em; padding: 6px;} #pushButton_2:hover { background-color: #1260be;}");
    this->ui->pushButton_3->setStyleSheet("#pushButton_3 {text-align:left; background-color: #26890c; color: white; border-style: outset; border-width: 2px; border-radius: 3px; border-color: #20750a; font: bold 14px; min-width: 10em; padding: 6px;} #pushButton_3:hover { background-color: #237e0b;}");
    this->ui->label_question->setStyleSheet("#label_question { background-color: white; font: bold 18px; min-width: 60px; min-height: 30px; max-height: 200px; padding: 6px; color: #333333; border-style: outset; border-color: #cfcfcf; border-width: 2px;}");
    this->ui->label_points->setStyleSheet("#label_points {color: #333333; font: bold 18px; min-width: 60 px; padding: 6px;}");
    this->ui->pushButton_again->setStyleSheet("#pushButton_again {background-color: #383040; color: white; border-style: outset; border-width: 2px; border-radius: 10px; border-color: black; font: bold 14px; min-width: 10em; padding: 6px;} #pushButton_again:hover { background-color: black;}");
    this->ui->pushButton_quit->setStyleSheet("#pushButton_quit {background-color: #383040; color: white; border-style: outset; border-width: 2px; border-radius: 10px; border-color: black; font: bold 14px; min-width: 10em; padding: 6px;} #pushButton_quit:hover { background-color: black;}");
    this->ui->label_result->setStyleSheet("#label_result { background-color: white; font: bold 18px; min-width: 60px; min-height: 30px; max-height: 200px; padding: 6px; color: #333333; border-style: outset; border-color: #cfcfcf; border-width: 2px;}");
    this->ui->label_question_progr->setStyleSheet("#label_question_progr { background-color: white; font: bold 18px; min-width: 60px; min-height: 30px; max-height: 200px; padding: 6px; color: #333333; }");

    connect(this->ui->pushButton_quit,&QPushButton::released,this, [=](){this->close();});
    connect(this->ui->pushButton_start,&QPushButton::released,this, &MainWindow::onStart);
    connect(this->ui->pushButton_again,&QPushButton::released,this, &MainWindow::onStart);
    connect(this->ui->lineEdit, &QLineEdit::returnPressed,this, &MainWindow::onStart);
    connect(this->timer, SIGNAL(timeout()),this, SLOT(onTimerTimeout()));    

    connect(this->ui->pushButton_0,&QPushButton::released,this, [this](){emit gotAnswer(0);});
    connect(this->ui->pushButton_1,&QPushButton::released,this, [this](){emit gotAnswer(1);});
    connect(this->ui->pushButton_2,&QPushButton::released,this, [this](){emit gotAnswer(2);});
    connect(this->ui->pushButton_3,&QPushButton::released,this, [this](){emit gotAnswer(3);});

    connect(this, &MainWindow::gotAnswer, this, &MainWindow::onGotAnswer);
    connect(this, &MainWindow::displayPreview, this, &MainWindow::onDisplayPreview);
    connect(this->timeLine, &QTimeLine::frameChanged, this->ui->progressBar, &QProgressBar::setValue);
    connect(this->timeLine, &QTimeLine::finished, this, &MainWindow::startAnimation);
    connect(this->animation_group,&QSequentialAnimationGroup::finished,this, &MainWindow::onDisplayQuestion);
    connect(this->animation_group,&QSequentialAnimationGroup::currentLoopChanged,this, [=](){ui->label_animated_intro->setNumber(ui->label_animated_intro->getNumber()-1);});
}


void MainWindow::startTimer()
{
    this->timer->start();
}
void MainWindow::stopTimer()
{
    this->timer->stop();
}
void MainWindow::resetTimer()
{
    time_left = game->time_limit;
}
void MainWindow::updateTimer()
{
    time_left--;
}

void MainWindow::onTimerTimeout()
{
    updateTimer();
    if (time_left == 0)
    {
        emit gotAnswer(-1);
    }
    else
    {
        this->ui->label_timer->setNumber(time_left);
        this->ui->label_timer->repaint();
    }
}

void MainWindow::onStart()
{
    QString input = ui->lineEdit->text();
    if (input!="")
        player_name=input;
    game->reset();
    current_question=0;
    emit displayPreview();
}

void MainWindow::onDisplayPreview()
{
    ui->label_question_progr->setText(game->getQuestion(current_question)->getQuestion());
    ui->stackedWidget->setCurrentIndex(4);
    timeLine->setStartFrame(0);
    timeLine->start();
}

void MainWindow::startAnimation()
{
    if (animation_group->currentLoop() == 2)
        this->ui->label_animated_intro->setNumber(4);
    else
        this->ui->label_animated_intro->setNumber(3);

    ui->stackedWidget->setCurrentIndex(3);
    animation_group->start();
}

void MainWindow::onGotAnswer(int ans)
{
    //reset Timer
    stopTimer();
    if(game->getQuestion(current_question-1)->isRightAnswer(ans))
    {
        game->increasePoints();
    }
    //show result
    if (current_question == game->n_questions)
    {
        QString msgText= player_name + ", you got " + QString::number(game->getPoints())+ " of " + QString::number(game->n_questions) + " points.";
        this->ui->label_result->setText(msgText);
        ui->stackedWidget->setCurrentIndex(2);
        return;
    }
    emit displayPreview();
}

void MainWindow::onDisplayQuestion()
{
    resetTimer();
    ui->stackedWidget->setCurrentIndex(1);
    Question * question = game->getQuestion(current_question++);
    q_check_ptr(question);
    this->ui->label_question->setText(question->getQuestion());
    this->ui->pushButton_0->setText(" " + question->getAlternative(0));
    this->ui->pushButton_1->setText(" " + question->getAlternative(1));
    this->ui->pushButton_2->setText(" " + question->getAlternative(2));
    this->ui->pushButton_3->setText(" " + question->getAlternative(3));
    //show timer
    this->ui->label_timer->setNumber(time_left);
    this->ui->label_timer->repaint();
    //show points
    this->ui->label_points->setText(QString::number(game->getPoints()) + "\nAnswers ");
    //start Timer
    startTimer();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete animation_1;
    delete animation_2;
    delete animation_group;
    delete timeLine;
    delete game;
    delete timer;
}
