#include "roundlabel.h"
#include <QPainter>
#include <QFontDatabase>

RoundLabel::RoundLabel(QWidget *parent) :
    QLabel(parent)
{
//    setBackgroundRole(QPalette::Base);
//    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    int font_id = QFontDatabase::addApplicationFont(":/Montserrat-Bold.ttf");
    QString font_str = QFontDatabase::applicationFontFamilies(font_id).at(0);
    font = QFont(font_str,QFont::Normal);
};

void RoundLabel::setNumber(int number) {this->number = number;}
//QSize RoundLabel::minimumSizeHint() const
//{
//    return QSize(50, 50);
//}

//QSize RoundLabel::sizeHint() const
//{
//    return QSize(180, 180);
//}

void RoundLabel::paintEvent(QPaintEvent *e)
{
//    QPixmap pixmap(QSize(this->width(), this->height()));
//    QPainter p(&pixmap);
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing, true);
    p.setPen(QColor("#864cbf"));
    p.setBrush(QColor("#864cbf"));
    QRect rect(0, 0, width(), height());
    p.drawEllipse(rect);
    p.setPen(Qt::white);
    font.setPixelSize(height()/3);
    p.setFont(font);
    QFontMetrics fontMetrics(font);
    //QRect text_Rect = rect.adjusted(-fontMetrics.leftBearing('1'), 0, fontMetrics.leftBearing('1'), 0);

    p.drawText(rect, Qt::AlignCenter, QString::number(number));
    //this->setPixmap(pixmap.scaled(this->size(), Qt::KeepAspectRatio,Qt::SmoothTransformation));
}

//ui->label->setPixmap( pix.scaled( ui->label->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation)
