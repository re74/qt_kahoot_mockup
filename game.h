#ifndef GAME_H
#define GAME_H
#include "qobjectdefs.h"
#include "question.h"

class Game
{   
    private:
        int points=0;        
        Question *questions;

    public:
        const int time_limit;
        const int n_questions;
        Game(Question *questions, int n_questions, int time_limit);
        void reset();
        void increasePoints();
        int getPoints();
        Question *getQuestion(int n);
};

#endif // GAME_H
