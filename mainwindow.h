#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QTimeLine>
#include <QDebug>
#include "game.h"
#include "qpropertyanimation.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

signals:
    void startGame();
    void displayPreview();
    void gotAnswer(int ans);
    void displayQuestion();

public:
    explicit MainWindow(Game *g , QWidget *parent = nullptr);
    ~MainWindow();
    Game *game;
    QTimer *timer;
    QTimeLine *timeLine;
    QPropertyAnimation *animation_1;
    QPropertyAnimation *animation_2;
    QSequentialAnimationGroup * animation_group;
    QString player_name = "Player";


private:
    int current_question;
    int time_left;
    Ui::MainWindow *ui;
    void startTimer();
    void stopTimer();
    void resetTimer();
    void updateTimer();

private slots:
    void onDisplayQuestion();
    void onTimerTimeout();
    void onStart();
    void startAnimation();
    void onGotAnswer(int ans);
    void onDisplayPreview();
};

#endif // MAINWINDOW_H
