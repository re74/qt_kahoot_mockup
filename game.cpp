#include "game.h"

Game::Game(Question *questions, int n_questions, int time_limit) :  time_limit(time_limit), n_questions(n_questions)
{
    this->questions = questions;
}
void Game::reset()
{
    points=0;
}
void Game::increasePoints()
{
    ++points;
}
int Game::getPoints()
{
    return points;
}
Question * Game::getQuestion(int n)
{
    if (n >= n_questions || n < 0)
        return nullptr;
    return &(questions[n]);
}

