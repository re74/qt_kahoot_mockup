#ifndef ANIMATEDINTRO_H
#define ANIMATEDINTRO_H

#include <QMainWindow>
#include <QLabel>
#include <QDebug>

class AnimatedIntro : public QLabel
{
    Q_OBJECT
    Q_PROPERTY(int rotation READ rotation WRITE setRotation);
    Q_PROPERTY(int scale READ scale WRITE setScale);
    QFont font;
    int m_rotation;
    int m_scale;
    int number=3;
public:

    explicit AnimatedIntro(QWidget *parent = nullptr);
    ~AnimatedIntro() {};
    int rotation() const;
    int scale() const;


    int getNumber() const;
    void setNumber(int newNumber);

public slots:

    void setRotation(int r) { m_rotation = r; update(); }
    void setScale(int s) {m_scale = s; update();}

protected :
    virtual void paintEvent(QPaintEvent * e) override;
};

#endif // ANIMATEDINTRO_H
