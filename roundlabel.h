#ifndef ROUNDLABEL_H
#define ROUNDLABEL_H

#include <QMainWindow>
#include <QLabel>
#include <QDebug>

class RoundLabel : public QLabel
{
    Q_OBJECT
    int number;
    QFont font;
public:
    explicit RoundLabel(QWidget *parent = nullptr);
    ~RoundLabel() {};
    void setNumber(int number);
//    QSize minimumSizeHint() const override;
//    QSize sizeHint() const override;

protected :
    virtual void paintEvent(QPaintEvent * e) override;
};

#endif

