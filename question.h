#ifndef QUESTION_H
#define QUESTION_H
#include <QString>

class Question
{
    QString question;
    QString alternatives[4];
    int right_alt;
public:
    Question(QString question, QString alternatives[4], int right_alt);
    bool isRightAnswer(int ans);
    QString getQuestion();
    QString getAlternative(int n);
};

#endif // QUESTION_H
