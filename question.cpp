#include "question.h"

Question::Question(QString question, QString alternatives[4], int right_alt) : question(question), right_alt(right_alt)
{
    for (int i = 0; i < 4 ; i++)
    {
        this->alternatives[i]=alternatives[i];
    }
}
bool Question::isRightAnswer(int ans)
{
    return (ans == right_alt);
}
QString Question::getQuestion(){return question;}
QString Question::getAlternative(int n){return alternatives[n];}
